## Introduction
Datazoom is a high availability real-time data collection solution. This android framework project builds the framework file that will be distributed. This document summarizes how to integrate the DataZoom framework with applications.

## Exo Collector library in your android application
The Firetv ExoPlayer framework allows access to the Exoplayer included with the Android operating system. Datazoom’s ExoCollector facilitates exo android applications to send video playback events based on the configuration created in data-pipes.

### Adding dependency to your project
1. Add following maven repository url in your project's build.gradle file.

```
repositories {
maven {
    url 'https://gitlab.com/datazoom/firetv-group/firetv-collector-libraries-release/raw/master'
}
}
```

2. Add following dependency in your project's build.gradle file.

```
dependencies {
    implementation 'com.datazoom.android:exo-collector:1.2.0'
}
```
3. Add retrofit library to your project if you don't have it already.

```
dependencies {
    implementation 'com.squareup.retrofit2:retrofit:2.4.0'
    implementation 'com.google.code.gson:gson:2.8.5'
}
```
4. Add following compile options if you don't have it already

```
compileOptions {
    sourceCompatibility JavaVersion.VERSION_1_8
    targetCompatibility JavaVersion.VERSION_1_8
}
```
### Calling ExoCollector with configurations
Use the following code snippet to add ExoCollector to your project.

```
String configId = <configuration id from Datazoom>
String configUrl = <url given by Datazoom>
ExoPlayerCollector.create(player, MainActivity.this)
                    .setConfig(new DatazoomConfig(configId, configUrl))
                    .connect(new DZBeaconConnector.ConnectionListener(){

                        @Override
                        public void onSuccess() {
                            //Everything is setup, connector initialization complete.
                        }
                        
                        @Override
                        public void onError(Throwable t) {
                            //Error occurred while connector initialization.
                        }
 
                    });
```
### Run the app 

## Demo Application

A demo application that shows the usage of this framework is available **[Here](https://gitlab.com/datazoom/firetv-group/firetv-exo-demo)** .

## Credit
  - Veena B K

## Link to License/Confidentiality Agreement
Datazoom, Inc ("COMPANY") CONFIDENTIAL
Copyright (c) 2017-2018 [Datazoom, Inc.], All Rights Reserved.
NOTICE:  All information contained herein is, and remains the property of COMPANY. The intellectual and technical concepts contained
herein are proprietary to COMPANY and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
from COMPANY.  Access to the source code contained herein is hereby forbidden to anyone except current COMPANY employees, managers or contractors who have executed
Confidentiality and Non-disclosure agreements explicitly covering such access.
The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
information that is confidential and/or proprietary, and is a trade secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

